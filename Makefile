CC=gcc
CFLAGS=-std=gnu99 -Wall -Wextra -pedantic -DNDEBUG
PROG=testovac
VER=$(PROG)-0.1
# XXX change if no such directory
RPMDIR=~/rpmbuild
RPMSRC=$(RPMDIR)/SOURCES

all: $(PROG)

$(PROG): testovac.c rtt_context.c rtt_context.h
	$(CC) $(CFLAGS) testovac.c rtt_context.c -o $@ -lm -lpthread

clean:
	rm -f $(PROG)

clean-rpm:
	rm -f $(VER).tar.gz
	rm -rf $(VER)
	rm -rf $(RPMDIR)/*/$(PROG)*

clean-all: clean clean-rpm

pack:
	rm -f xsemri00.tar
	tar -cf xsemri00.tar *.c *.h $(PROG).1 Makefile

# creating archive and copying it to rpmbuild directory
package:
	mkdir -p $(VER)
	cp *.{c,h} $(PROG).{1,spec} Makefile $(VER)/
	tar -zcvf $(VER).tar.gz $(VER)
	rm -rf $(VER)

# building rpm from spec file
rpm: clean-all package
	cp $(VER).tar.gz $(RPMSRC)
	rpmbuild -ba $(PROG).spec
