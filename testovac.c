/// Jakub Semric
/// BUT FIT 2017

#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpointer-to-int-cast"
#pragma GCC diagnostic ignored "-Wincompatible-pointer-types"

#define _BSD_SOURCE

#include <time.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <errno.h>
#include <stdbool.h>
#include <assert.h>
#include <pthread.h>
#include <getopt.h>
#include <signal.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netinet/icmp6.h>
#include <netinet/ip6.h>
#include <netdb.h>
#include <linux/ip.h>
#include <linux/icmp.h>
#include <unistd.h>

#include "rtt_context.h"

#define BUFSIZE 4096
#define REPORT_TIME 3600

// macros for icmp headers manipulation
#define ICMP6_GET_SEQ(packet) \
        ((struct icmp6_hdr*)packet)->icmp6_dataun.icmp6_un_data16[1]

#define ICMP6_GET_ID(packet) \
        ((struct icmp6_hdr*)packet)->icmp6_dataun.icmp6_un_data16[0]

#define ICMP6_GET_CKSUM(packet) \
        ((struct icmp6_hdr*)packet)->icmp6_cksum

#define ICMP6_GET_CODE(packet) \
        ((struct icmp6_hdr*)packet)->icmp6_code

#define ICMP6_GET_TYPE(packet) \
        ((struct icmp6_hdr*)packet)->icmp6_type

#define ICMP_GET_SEQ(packet) \
        ((struct icmphdr*)packet)->un.echo.sequence

#define ICMP_GET_ID(packet) \
        ((struct icmphdr*)packet)->un.echo.id

#define ICMP_GET_CKSUM(packet) \
        ((struct icmphdr*)packet)->checksum

#define ICMP_GET_CODE(packet) \
        ((struct icmphdr*)packet)->code

#define ICMP_GET_TYPE(packet) \
        ((struct icmphdr*)packet)->type

#define GET_SEC(end, beg) \
        (end.tv_sec - beg.tv_sec)

#define GET_MSEC(end, beg) \
        (GET_SEC(end, beg) * 1000 + (end.tv_usec - beg.tv_usec) / 1000.0)

// global variables for communication between threads

// boolean indicating an error
bool no_error = false;

// tasks to run in parallel
pthread_t *tasks = NULL;
int task_count = 0;

// mutex for stoping the threads
pthread_mutex_t stopth_lock;

// options, -1 -> not set
bool udp = false;
int data_size = 56;    // bytes
int ploss_time = 300;   // seconds
int echo_freq = 100;   // milliseconds
int wtime = -1;        // seconds
int port = -1;
float rtt = -1;          // milliseconds
bool verbose = false;
int lisport = -1;

const char* helpstr =
"usage: nodetester [OPTIONS] NODE1 [NODE2 ...]\n"
"options:\n"
"  -h            : show this help and exit\n"
"  -u            : use UDP echo requests\n"
"  -s <size>     : size of data to send, default value is 56B\n"
"  -t <interval> : packet loss output frequency, default value is 300s\n"
"  -i <interval> : frequency of sending the messages in milliseconds,\n"
"                  default 100 ms\n"
"  -w <timeout>  : time to wait for a response, default value 2s\n"
"  -p <port>     : UDP port number\n"
"  -l <port>     : listen to port as UDP echo server\n"
"  -r <value>    : RTT value\n"
"  -v            : verbose mode\n";

// copied from https://cboard.cprogramming.com/networking-device-communication/
// /71459-question-icmp-echo-request.html
int32_t checksum(uint16_t *buf, int32_t len)
{
    int32_t nleft = len;
    int32_t sum = 0;
    uint16_t *w = buf;
    uint16_t answer = 0;

    while(nleft > 1)
    {
        sum += *w++;
        nleft -= 2;
    }

    if(nleft == 1)
    {
        *(uint16_t *)(&answer) = *(uint8_t *)w;
        sum += answer;
    }

    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    answer = ~sum;

    return answer;
}

void set_random_data(unsigned char *data, size_t length)
{
    for (size_t i = 0; i < length; i++) {
        data[i] = rand() % 256;
    }
}

bool is_same_data(const unsigned char *d1, int len1, const unsigned char *d2,
                  int len2)
{
    if (len1 != len2) {
        return false;
    }

    return memcmp(d1, d2, len1) == 0;
}

// checking seqno, id and data of response
bool check_response(const unsigned char *reply, int rlen,
                    const unsigned char *send, int slen, int net_family)
{
    assert(net_family == AF_INET || net_family == AF_INET6);

    if (!udp) {
        int hdr_len;
        if (net_family == AF_INET6) {
            hdr_len = sizeof(struct icmp6_hdr);
            if (ICMP6_GET_SEQ(reply) != ICMP_GET_SEQ(send) ||
                ICMP6_GET_ID(reply) != ICMP6_GET_ID(send))
            {
                return false;
            }
        }
        else {
            struct iphdr *ipr = (struct iphdr*)reply;
            int len = ipr->ihl << 2;
            hdr_len = sizeof(struct icmphdr);
            reply += len;
            rlen -= len;
            if (ICMP6_GET_SEQ(reply) != ICMP_GET_SEQ(send) ||
                ICMP6_GET_ID(reply) != ICMP6_GET_ID(send))
            {
                return false;
            }
        }
        reply += hdr_len;
        rlen -= hdr_len;
        send += hdr_len;
        slen -= hdr_len;
    }

    return is_same_data(reply, rlen, send, slen);
}


// increase seqno and edit data
void set_packet(unsigned char *packet, int net_family)
{
    assert(net_family == AF_INET || net_family == AF_INET6);
    int hdr_len = 0;

    if (!udp) {
        if (net_family == AF_INET) {
            hdr_len = sizeof(struct icmphdr);
            int seq = ntohs(ICMP_GET_SEQ(packet)) + 1;
            ICMP_GET_SEQ(packet) = htons(seq);
            ICMP_GET_CKSUM(packet) = 0;
            set_random_data(packet + hdr_len, data_size);
            ICMP_GET_CKSUM(packet) = checksum((uint16_t *)packet,
                                              hdr_len + data_size);
        }
        else {
            hdr_len = sizeof(struct icmp6_hdr);
            int seq = ntohs(ICMP_GET_SEQ(packet)) + 1;
            ICMP6_GET_SEQ(packet) = htons(seq);
            ICMP6_GET_CKSUM(packet) = 0;
            set_random_data(packet + hdr_len, data_size);
            ICMP6_GET_CKSUM(packet) = checksum((uint16_t *)packet,
                                               hdr_len + data_size);
        }
    }
    else {
        set_random_data(packet + hdr_len, data_size);
    }
}

// initialize icmp packet, for udp needless
int init_packet(unsigned char *packet, int len, int net_family)
{
    assert(net_family == AF_INET || net_family == AF_INET6);
    int hdr_len = 0;

    if (!udp) {
        if (net_family == AF_INET) {
            hdr_len = sizeof(struct icmphdr);
            ICMP_GET_TYPE(packet) = ICMP_ECHO;
            ICMP_GET_CODE(packet) = 0;
            ICMP_GET_SEQ(packet) = 0;
            ICMP_GET_ID(packet) = htons((int)packet & 0xFFFF);
            ICMP_GET_CKSUM(packet) = 0;
        }
        else {
            hdr_len = sizeof(struct icmp6_hdr);
            ICMP6_GET_TYPE(packet) = ICMP6_ECHO_REQUEST;
            ICMP6_GET_CODE(packet) = 0;
            ICMP6_GET_SEQ(packet) = 0;
            ICMP6_GET_ID(packet) = htons((int)packet & 0xFFFF);
            ICMP6_GET_CKSUM(packet) = 0;
        }
    }

    assert(hdr_len + data_size <= len);

    return hdr_len + data_size;
}

// core function, works with both icmp(v6) and udp
void ping(rtt_context_t *context)
{
    int net_family;
    void *dst_addr;
    int addrlen = 0;
    // getting ip
    struct sockaddr_in node_addr;
    struct sockaddr_in6 node_addr6;
    struct addrinfo hint, *res = 0;
    struct hostent *h;
    memset(&hint, 0, sizeof(hint));
    hint.ai_family = PF_UNSPEC;

    // resolving ipv4 or ipv6
    if (getaddrinfo(context->hostname, 0, &hint, &res)) {
        fprintf(stderr, "Error: invalid hostname\n");
        return;
    }

    net_family = res->ai_family;
    // it is ipv4
    if (res->ai_family == AF_INET) {
        if (!(h = gethostbyname(context->hostname))) {
            fprintf(stderr, "Error: invalid hostname\n");
            return;
        }

        bzero((char*)&node_addr, sizeof(node_addr));
        node_addr.sin_family = AF_INET;
        node_addr.sin_port = 0;

        // set udp port
        if (udp) {
            node_addr.sin_port = htons((unsigned short)port);
        }

        memmove((char*) &node_addr.sin_addr, (char *)h->h_addr_list[0],
                h->h_length);
        dst_addr = &node_addr;
        addrlen = sizeof(node_addr);
    }
    else if (res->ai_family == AF_INET6) {
        if (!(h = gethostbyname2(context->hostname, AF_INET6))) {
            fprintf(stderr, "Error: invalid hostname\n");
            return;
        }

        bzero((char*)&node_addr6, sizeof(node_addr6));
        node_addr6.sin6_flowinfo = 0;
        node_addr6.sin6_family = AF_INET6;

        // set udp port
        if (udp) {
            node_addr6.sin6_port = htons((unsigned short)port);
        }

        memmove((char*) &node_addr6.sin6_addr.s6_addr, (char *)h->h_addr_list[0],
                h->h_length);
        dst_addr = &node_addr6;
        addrlen = sizeof(node_addr6);
    }
    else {
        fprintf(stderr, "Error: invalid hostname\n");
        return;
    }

    int type = udp ? SOCK_DGRAM : SOCK_RAW;
    int proto = udp ? 0 :
                     ( net_family == AF_INET ? IPPROTO_ICMP : IPPROTO_ICMPV6);
    if ((context->sockdes = socket(net_family, type, proto)) < 0) {
        perror("Error: socket");
        return;
    }

    if (!udp) {
        if (net_family == AF_INET) {
            // specific options for ICMP(v4)
            struct icmp_filter filter;
            filter.data = ~(1<<ICMP_ECHOREPLY);
            if (setsockopt(context->sockdes, SOL_RAW, ICMP_FILTER,(char*)&filter,
                sizeof(struct icmp_filter)) < 0)
            {
                perror("Error: icmp filter");
                return;
            }
        }
        else {
            // specific options for ICMPv6
            struct icmp6_filter filter;
            ICMP6_FILTER_SETBLOCKALL(&filter);
            ICMP6_FILTER_SETPASS(ICMP6_ECHO_REPLY, &filter);
            if (setsockopt(context->sockdes, IPPROTO_ICMPV6, ICMP6_FILTER, (char*)&filter,
                sizeof(struct icmp6_filter)) < 0)
            {
                perror("Error: icmpv6 filter");
                return;
            }
        }
    }

    // set packet receiving timeout
    struct timeval tv;
    // recv -w not set, timeout is 2 by default or 2*rtt
    if (wtime == -1) {
        tv.tv_sec = context->rtt_bound > 0 ? (int)(2 * context->rtt_bound / 1000) : 2;
        tv.tv_usec = context->rtt_bound > 0 ?
                     (int)(2 * 1000 * context->rtt_bound) : 0;
    }
    else {
        // timeout was set
        tv.tv_sec = wtime;
        tv.tv_usec = 0;
    }

    if (setsockopt(context->sockdes, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        perror("Error: recv timeout");
        return;
    }

    if (connect(context->sockdes, dst_addr, addrlen) < 0)
    {
        perror("Error");
        return;
    }

    // sending echo requests and computing statistics

    unsigned char buf[BUFSIZE] = {0};
    unsigned char resp[BUFSIZE] = {0};
    int len = init_packet(buf, BUFSIZE, net_family);

    unsigned long inqueue = 0;
    float rtt_cur;
    struct timeval rtt_beg, rtt_end;
    struct timeval report_beg, report_end;
    struct timeval ploss_beg, ploss_end;

    gettimeofday(&report_beg, NULL);
    gettimeofday(&ploss_beg, NULL);

    // start computing
    while (true) {
        set_packet(buf, net_family);
        usleep(echo_freq*1000);
        // remove queued packets
        while (inqueue) {
            if (recv(context->sockdes, resp, BUFSIZE, MSG_DONTWAIT) > 0) {
                inqueue--;
            }
            else {
                break;
            }
        }

        bool lost = false;
        bool rtt_elapsed = false;
        int bytes = write(context->sockdes, buf, len);
        if (bytes < 0) {
            perror("Error: send");
            return;
        }

        gettimeofday(&rtt_beg, NULL);
        bytes = read(context->sockdes, resp, BUFSIZE);

        gettimeofday(&rtt_end, NULL);
        rtt_cur = GET_MSEC(rtt_end, rtt_beg);
        assert (rtt_cur >= 0);
        if (bytes < 0) {
            // timeout elapsed
            lost = true;
            if (errno != EAGAIN) {
                // other error
                perror("Error: receive");
                return;
            }
        }
        else if (check_response(resp, bytes, buf, len, net_family)) {
            // check if echo reply is ok
            inqueue = 0;
            context_add_rtt(context, rtt_cur);

            if (context->rtt_bound > 0 && context->rtt_bound < rtt_cur) {
                // rtt max has been reached -> packet lost
                lost = true;
                rtt_elapsed = true;
            }
            // response is ok, continue
        }
        else {
            // wrong contain of echo reply
            // before send it tries to read again, maybe the reply is now in queue
            inqueue++;
            lost = true;
        }

        context_packet_lost(context, lost, rtt_elapsed);

        if (verbose && !lost) {
            context_write_verbose(context, rtt_cur, bytes);
        }

        // statistics output
        gettimeofday(&report_end, NULL);
        if (GET_SEC(report_end, report_beg) >= REPORT_TIME) {
            context_write_stats(context);
            gettimeofday(&report_beg, NULL);
        }
        // packet loss report
        gettimeofday(&ploss_end, NULL);
        if (GET_SEC(ploss_end, ploss_beg) >= ploss_time) {
            gettimeofday(&ploss_beg, NULL);
            context_write_packet_loss(context);
        }
    }
}

void udp_echo_server(rtt_context_t *context)
{
    context->sockdes = socket(AF_INET, SOCK_DGRAM, 0);

    if (context->sockdes < 0) {
        perror("Error: ");
        return;
    }

    int optval = 1;
    if (setsockopt(context->sockdes, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int))) {
        perror("Error sockopt");
        return;
    }

    struct sockaddr_in addr, client_addr;
    bzero((char*)&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons((unsigned short)lisport);

    if (bind(context->sockdes, (struct sockaddr*)&addr, sizeof(addr)) < 0)
    {
        perror("Error: ");
        return;
    }

    int bytes;
    char buf[4096] = {0};

    while (true) {

        int client_len = sizeof(client_addr);
        bytes = recvfrom(context->sockdes, buf, 256, 0, (struct sockaddr*)&client_addr,
                         (void*)&client_len);
        if (bytes < 0) {
            perror("Error: ");
            return;
        }

        if (sendto(context->sockdes, buf, bytes, 0, (struct sockaddr*)&client_addr,
                   sizeof(client_addr)) < 0)
        {
            perror("Error: ");
            return;
        }

        memset(buf, 0, bytes);
    }
}

// function for checking input
unsigned str2uint(const char *buf) {
    char *endptr;
    int res = strtoul(buf, &endptr, 10);
    if (*endptr) {
        fprintf(stderr, "Error: invalid argument value\n");
        exit(1);
    }

    return res;
}

// function for checking input
float str2ufloat(const char *buf) {
    char *endptr;
    float res = strtof(buf, &endptr);
    if (*endptr || res < 0) {
        fprintf(stderr, "Error: invalid argument value\n");
        exit(1);
    }

    return res;
}

void stop_all_threads(bool to_exit) {
    pthread_mutex_lock(&stopth_lock);
    pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
    for (int i = 0; i < task_count; i++) {
        pthread_cancel(tasks[i]);
    }
    if (to_exit) {
        exit(1);
    }
    pthread_mutex_unlock(&stopth_lock);
}

// functions to be run as threads
void *udp_serve(void *arg) {
    udp_echo_server((rtt_context_t *)arg);
    stop_all_threads(true);
    return NULL;
}

void *test_node(void *arg) {
    ping((rtt_context_t *)arg);
    // if thread was here it would exited with an error
    stop_all_threads(true);
    return NULL;
}


// clean up before exit
void sighandl(int signal) {
    puts("");
    stop_all_threads(false);
    no_error = true;
}

int main(int argc, char **argv) {

    if (argc == 1) {
        fprintf(stderr, "%s", helpstr);
        return 0;
    }

    int c;
    // number of options
    int opt_cnt = 1;
    // handle arguments
    while ((c = getopt(argc, argv, "huvt:i:p:l:s:r:w:")) != -1) {
        opt_cnt += 2;
        switch (c) {
            case 'h':
                fprintf(stderr, "%s", helpstr);
                return 0;
            case 'u':
                udp = true;
                opt_cnt--;
                break;
            case 'v':
                verbose = true;
                opt_cnt--;
                break;
            case 't':
                ploss_time = str2uint(optarg);
                break;
            case 'i':
                echo_freq = str2uint(optarg);
                break;
            case 'p':
                port = str2uint(optarg);
                break;
            case 'l':
                lisport = str2uint(optarg);
                break;
            case 'r':
                rtt = str2ufloat(optarg);
                break;
            case 's':
                data_size = str2uint(optarg);
                break;
            case 'w':
                wtime = str2uint(optarg);
                break;
            default:
                return 1;
        }
    }

    if (udp != (port != -1)) {
        fprintf(stderr, "options \"-u\" and \"-p\" must be set"
                " simultaneously\n");
        return 1;
    }

    int nodes = argc - opt_cnt;
    task_count = ((lisport != -1) + nodes);
    if ((tasks = malloc(sizeof(pthread_t)*task_count)) == 0) {
        perror("Error: malloc");
        return 1;
    }

    pthread_mutex_t lock;
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Error: mutex");
        goto FREE1;
    }

    if (pthread_mutex_init(&stopth_lock, NULL) != 0) {
        perror("Error: mutex");
        goto FREE2;
    }

    rtt_context_t **contexts =
        init_rtt_context_array(argv + opt_cnt, rtt, &lock, nodes,
                               task_count);

    if (contexts == NULL) {
        perror("Error: hostname");
        pthread_mutex_destroy(&stopth_lock);
        FREE2:
        pthread_mutex_destroy(&lock);
        FREE1:
        free(tasks);
        return 1;
    }

    signal(SIGINT, sighandl);

    int idx = 0;
    for (; idx < nodes; idx++) {
        pthread_create(&tasks[idx], 0, test_node, contexts[idx]);
    }

    if (lisport != -1) {
        pthread_create(&tasks[idx + 1], 0, udp_serve, contexts[nodes]);
    }

    for (int i = 0; i < task_count; i++) {
        pthread_join(tasks[i], NULL);
    }

    // if code reaches here some thread should have failed or SIGINT had been caught
    int code = 1;
    if (no_error) {
        code = 0;
        puts("---- rtt statistics ----");
        for (int i = 0; i < nodes; i++) {
            context_write_stats(contexts[i]);
        }
    }

    free(tasks);
    delete_rtt_context_array(contexts, task_count);
    pthread_mutex_destroy(&lock);
    pthread_mutex_destroy(&stopth_lock);

    return code;
}
