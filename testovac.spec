Name: testovac
Version: 0.1	
Release: 1%{?dist}
Summary: RTT and packet loss measurement

Group: Applications/Internet
License: GPL
Source0: testovac-0.1.tar.gz

%description
Program for testing availability of nodes and measuring RTT.

%prep
%setup -q

%build
make

%install
rm -rf %{buildroot}
install --directory %{buildroot}/usr/bin
install --directory %{buildroot}/usr/share/man/man1
install -m 0755 testovac %{buildroot}/usr/bin/testovac
install -m 0755 testovac.1 %{buildroot}/usr/share/man/man1/testovac.1

%clean
rm -rf %{buildroot}

%files
/usr/bin/testovac
/usr/share/man/man1/testovac.1.gz
