#ifndef RTT_CONTEXT
#define RTT_CONTEXT

#include <stdbool.h>
#include <pthread.h>

typedef struct {
    char *hostname;
    int sockdes;
    float rtt_max;
    float rtt_min;
    float rtt_sum;
    float rtt_sum2;
    float rtt_bound;
    unsigned long total_packets;
    unsigned long lost_packets;
    unsigned long rtt_elapsed;
    pthread_mutex_t *mutex;
} rtt_context_t;

rtt_context_t *init_rtt_context(const char *host, float rtt, pthread_mutex_t *mutex);

rtt_context_t **init_rtt_context_array(const char **hosts, float rtt, pthread_mutex_t *mutex, int hosts_size, int size);

void delete_rtt_context(rtt_context_t *context);

void delete_rtt_context_array(rtt_context_t **context, int size);

void context_add_rtt(rtt_context_t *context, float rtt_cur);

void context_packet_lost(rtt_context_t *context, bool lost, bool rtt_elapsed);

void context_write_stats(rtt_context_t *context);

void context_write_verbose(rtt_context_t *context, float rtt_cur, int bytes);

void context_write_packet_loss(rtt_context_t *context);

void timestamp_mprintf(pthread_mutex_t *mutex, const char *fmt, ...);

#endif
