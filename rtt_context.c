#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <stdarg.h>
#include <sys/time.h>
#include <assert.h>

#include "rtt_context.h"

rtt_context_t *init_rtt_context(const char *host, float rtt, pthread_mutex_t *mutex) {

    // split rtt part and hostname
    int str_size = strlen(host) + 1;
    float rtt_bound = rtt;
    char *rtt_suffix = strrchr(host, ';');
    if (rtt_suffix) {
        str_size = rtt_suffix - host;
        char *endptr;
        rtt_bound = strtof(rtt_suffix + 1, &endptr);
        if (*endptr || rtt_bound < 0) {
            // invalid host format
            return NULL;
        }
    }

    // copying host name
    char *hostname = calloc(str_size, 1);
    if (hostname == NULL) {
        return NULL;
    }

    // removing ';' or just copy
    strncpy(hostname, host, str_size);

    rtt_context_t *context = calloc(1, sizeof(rtt_context_t));
    if (context == NULL) {
        free(hostname);
        return NULL;
    }

    context->hostname = hostname;
    context->sockdes = -1;
    context->rtt_min = 100000;
    context->rtt_bound = rtt_bound;
    context->mutex = mutex;

    return context;
}

void delete_rtt_context(rtt_context_t *context) {
    if (context == NULL) {
        return;
    }

    if (context->hostname) {
        free(context->hostname);
    }

    if (context->sockdes > 0) {
        close(context->sockdes);
    }

    free(context);
}

void context_add_rtt(rtt_context_t *context, float rtt_cur) {
    assert(context);
    assert(rtt_cur >= 0);
    context->rtt_sum += rtt_cur;
    context->rtt_sum2 += rtt_cur*rtt_cur;
    if (context->rtt_max < rtt_cur) {
        context->rtt_max = rtt_cur;
    }

    if (context->rtt_min > rtt_cur) {
        context->rtt_min = rtt_cur;
    }
}

void context_packet_lost(rtt_context_t *context, bool lost, bool rtt_elapsed) {
    context->lost_packets += lost;
    context->rtt_elapsed += rtt_elapsed;
    context->total_packets++;
}

void context_write_stats(rtt_context_t *context) {
    if (context->total_packets <= context->lost_packets) {
        timestamp_mprintf(context->mutex, "%s: status down\n", context->hostname);
    }
    else {
        float avg = context->rtt_sum/context->total_packets;
        float mdev = sqrt(context->rtt_sum2/context->total_packets - avg*avg);
        assert(mdev >= 0);
        timestamp_mprintf(context->mutex, "%s: %0.2f%% packet loss, rtt min/avg/max/mdev"
                          " %0.3f/%0.3f/%0.3f/%0.3f ms\n", context->hostname,
                          100.0*context->lost_packets/context->total_packets,
                          context->rtt_min, avg, context->rtt_max, mdev);
    }
}

void context_write_verbose(rtt_context_t *context, float rtt_cur, int bytes)
{
    timestamp_mprintf(context->mutex, "%d bytes from %s time=%0.3f ms\n", bytes,
                      context->hostname, rtt_cur);
}

void context_write_packet_loss(rtt_context_t *context)
{
    if (context->rtt_elapsed) {
        timestamp_mprintf(context->mutex, "%s: %0.2f%% (%lu) packets exceeded RTT "
                          "threshold %0.3fms\n", context->hostname,
                          100.0*context->lost_packets/context->total_packets,
                          context->rtt_elapsed, context->rtt_bound);
    }
    else {
        timestamp_mprintf(context->mutex, "%s: %0.2f%% packet loss, %lu packet lost\n",
                          context->hostname,
                          100.0*context->lost_packets/context->total_packets,
                          context->lost_packets);
    }
}

// writes like printf, but with time-stamp and mutual access protection
void timestamp_mprintf(pthread_mutex_t *mutex, const char *fmt, ...) {
    assert(mutex);
    pthread_mutex_lock(mutex);

    struct timeval cur_time;
    gettimeofday(&cur_time, 0);
    int ms = cur_time.tv_usec / 10000;
    char buf[80];
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", localtime(&cur_time.tv_sec));
    printf("%s.%d ", buf, ms);
	va_list args;
	va_start(args, fmt);
	vfprintf(stdout, fmt, args);
	va_end(args);

    pthread_mutex_unlock(mutex);
}

void delete_rtt_context_array(rtt_context_t **context_arr, int size) {
    for (int i = 0; i < size; i++) {
        delete_rtt_context(context_arr[i]);
    }
    free(context_arr);
}

// creates an array of rtt contexts
// hosts don't have to be same size as result array
rtt_context_t **
init_rtt_context_array(const char **hosts, float rtt, pthread_mutex_t *mutex,
                       int hosts_size, int size)
{
    rtt_context_t **context_array = calloc(size, sizeof(rtt_context_t*));
    if (context_array == NULL) {
        return NULL;
    }
    for (int i = 0; i < size; i++) {
        const char *host = "";
        if (i < hosts_size) {
            host = hosts[i];
        }

        if ((context_array[i] = init_rtt_context(host, rtt, mutex)) == NULL)
        {
            for (int j = 0; j < i; j++) {
                delete_rtt_context(context_array[j]);
            }
            free(context_array);
        }
    }

    return context_array;
}
